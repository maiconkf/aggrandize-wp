var app = {};

app.init = function() {
	app.menu();
	app.animation();
	app.slider();
	app.modal();
};

app.menu = function() {
	var tmrSubMenu;

	$(".button-collapse").sideNav({
		edge: 'right',
		closeOnClick: true,
		draggable: true
	});

	$(document).on('mouseenter', '.dropdown-menu', function() {
		var type = $(this).data('type');
		$('.'+type).fadeIn(600);
	}).on('mouseleave', '.dropdown-menu', function() {
		tmrSubMenu = setTimeout(function() { $('.sub-nav').stop().fadeOut(); }, 100);
	}).on('mouseenter', '.sub-nav', function() {
		clearTimeout(tmrSubMenu);
	}).on('mouseleave', '.sub-nav', function() {
		$(this).stop().fadeOut();
	});

	var href = window.location.href;

	$('#categories a[href="'+href+'"]').addClass('selected');
};

app.animation = function() {
	window.sr = ScrollReveal();
	sr.reveal('.title', {duration: 600});
	sr.reveal('.subtitle, .socios .col', {duration: 700, delay: 200});
	sr.reveal('.service, .services .btn', {duration: 600, delay: 200});
	sr.reveal('#posts article', {duration: 700});
	sr.reveal('.solucao', {duration: 600, scale: 1});
	sr.reveal('.solucao-2, .solucao-5, .solucao-8', {delay: 150});
	sr.reveal('.solucao-3, .solucao-6, .solucao-9', {delay: 300});
};

app.slider = function() {
	$('#slider').slider();
};

app.modal = function() {
	var $modal = $('#modal'),
		winH = $(window).innerHeight();

	$('#solucao-contato .btn').off().on({
		'click' : function() {
			if(winH < 600) {
				$('body, html').stop().animate({ scrollTop: 0 }, 400);
				$('#modal .fade').css('height', $(document).innerHeight());
			}

			$modal.fadeIn();
		}
	});

	$('.close').off().on({
		'click' : function() {
			$modal.fadeOut();
		}
	});
};

$(function() {
	app.init();
});
