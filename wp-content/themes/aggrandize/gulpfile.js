var gulp = require('gulp'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	cssmin = require('gulp-cssmin'),
	stripCssComments = require('gulp-strip-css-comments'),
  newer = require('gulp-newer'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant');

var scss = ['./sass/**/*.scss'],
	  css  = ['./dist/css/materialize.css',
            './dist/css/fonts.css',
            './dist/css/reset.css',
            './dist/css/main.css'],
	  js	 = ['./js/vendor/jquery.min.js',
            './js/vendor/scrollreveal.min.js',
            './js/vendor/initial.js',
            './js/vendor/global.js',
            './js/vendor/velocity.min.js',
            './js/vendor/jquery.easing.1.3.js',
            './js/vendor/hammer.min.js',
            './js/vendor/jquery.hammer.js',
            './js/vendor/animation.js',
            './js/vendor/waves.js',
            './js/vendor/sideNav.js',
            './js/vendor/slider.js',
            './js/vendor/forms.js',
            './js/main.js'
            ],
    img  = './img/**/*';

gulp.task('scss', function() {
    gulp.src(scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('styles', function(){
    gulp.src(css)
    .pipe(concat('styles.min.css'))
    .pipe(stripCssComments({all: true}))
    .pipe(cssmin())
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('scripts', function() {
  gulp.src(js)
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('images', function() {
  // gulp.src('./img/**/*')
  //   // return gulp.src('./img/**/*')
  //   .pipe(newer('./dist/img/'))
  //   .pipe(imagemin({
  //     optimizationLevel: 5,
  //     progressive: true,
  //     verbose: true,
  //     use: [pngquant()]
  //   }))
  //   .pipe(gulp.dest('./dist/img/'));

  gulp.src(img)
      .pipe(newer('./dist/img/'))
      .pipe(imagemin({
        progressive: true,
        verbose: true,
        use: [pngquant()]
      }))
      .pipe(gulp.dest('./dist/img/'));
});

gulp.task('default',function() {
    gulp.src('./img/**/*').pipe(newer('./dist/img/')).pipe(imagemin({progressive: true, use: [pngquant()]})).pipe(gulp.dest('./dist/img/'));
    gulp.watch(scss, ['scss']);
    gulp.watch(css, ['styles']);
    gulp.watch(js, ['scripts']);
});