<?php
	/* Template name: Inicial */
	get_header();
?>
  	<section id="slider" class="slider">
	    <ul class="slides">
	      	<li>
		        <img src="<?php echo get_template_directory_uri() ?>/dist/img/home-solucoes-infra-oracle.jpg" class="responsive-img">
		        <div class="filter">
			        <div class="caption center-align">
			          <h3><strong>Soluções de Infraestrutura Oracle</strong></h3>
			          <h5 class="light grey-text text-lighten-3">Gestão, segurança, performance e alta disponibilidade com quem entende do assunto</h5>
			          <a href="<?php echo get_site_url(); ?>/solucoes" class="waves-effect waves-red btn-large red lighten-1">Saiba mais!</a>
			        </div>
		        </div>
	      	</li>
	      	<li>
		        <img src="<?php echo get_template_directory_uri() ?>/dist/img/home-healthcheck.jpg" class="responsive-img">
		        <div class="filter">
			        <div class="caption center-align">
			          <h3><strong>Solução HealthCheck</strong></h3>
			          <h5 class="light grey-text text-lighten-3">As principais variáveis de controle de seu ambiente de middleware Oracle em uma única visão</h5>
			          <a href="<?php echo get_site_url(); ?>/solucoes/healthcheck" class="waves-effect waves-red btn-large red lighten-1">Saiba mais!</a>
			        </div>
			    </div>
		    </li>
		    <li>
		        <img src="<?php echo get_template_directory_uri() ?>/dist/img/home-solucoes-devops.jpg" class="responsive-img">
		        <div class="filter">
			        <div class="caption center-align">
			          <h3><strong>Solução DevOps</strong></h3>
			          <h5 class="light grey-text text-lighten-3">Solução completa de DevOps, Continuous Delivery e Release Automation para Oracle</h5>
			          <a href="<?php echo get_site_url(); ?>/solucoes/devops" class="waves-effect waves-red btn-large red lighten-1">Saiba mais!</a>
			        </div>
			    </div>
	      	</li>
	    </ul>
  	</section>
  	<section id="content" class="clearfix home-content">
		<div class="col l12 center-block">
  			<div class="col l7 m11 center-block quote">
				<h1 class="about">Especialistas em infraestrutura Oracle com experiência em ambientes de alta criticidade.</h1>
			</div>
			<div class="separator"></div>
	  		<hgroup class="center title">
	  			<h2>SERVIÇOS</h2>
	  			<h3>Disponibilidade das informações, redução de risco de TI e desempenho dos aplicativos são fatores críticos para o sucesso dos negócios. Nossos serviços e produtos foram pensados de maneira a suportar o ecossistema da sua empresa com segurança, estabilidade e melhores práticas de TI.</h3>
	  		</hgroup>
	  		<div class="services clearfix">
	  			<div class="service col m6 l3 center">
	  				<div class="img consultoria"></div>
	  				<hgroup>
	  					<h2>Consultoria</h2>
	  					<h3>Certifique-se de que a infraestrutura Oracle está instalada, configurada, implantada, otimizada e mantida de acordo com as melhores práticas da Oracle.</h3>
	  				</hgroup>
	  			</div>
	  			<div class="service col m6 l3 center">
	  				<div class="img projetos"></div>
	  				<hgroup>
	  					<h2>Projetos</h2>
	  					<h3>Projetos de dimensionamento, instalação, configuração, migração, tuning, alta disponibilidade e disaster revovery de ambientes de infraestrutura Oracle.</h3>
	  				</hgroup>
	  			</div>
	  			<div class="service col m6 l3 center">
	  				<div class="img suporte"></div>
	  				<hgroup>
	  					<h2>Suporte</h2>
	  					<h3>De forma local ou remota, monitorar, antecipar, identificar causa raiz, remediar e corrigir problemas de toda infraestrutura Oracle.</h3>
	  				</hgroup>
	  			</div>
	  			<div class="service col m6 l3 center">
	  				<div class="img outsourcing"></div>
	  				<hgroup>
	  					<h2>Outsourcing</h2>
	  					<h3>Alocação de colaboradores especializados em infraestrutura Oracle.</h3>
	  				</hgroup>
	  			</div>
	  			<br clear="all">
	  		</div>
	  	</div>
  	</section>
	<section id="solucao-contato" class="home-footer">
		<div class="container">
			<div class="col m12 l10 center-block">
				<h2>Middleware - Banco de Dados - Siebel - Enterprise Manager - EBS - Retail - DEVOPS - FlexDeploy</h2>
			    <a href="<?php echo get_site_url(); ?>/solucoes" class="btn waves-effect waves-red btn-large red lighten-1">VER SOLUÇÕES</a>
			</div>
		</div>
	</section>
<?php get_footer(); ?>