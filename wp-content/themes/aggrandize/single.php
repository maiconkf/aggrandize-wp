<?php
	get_header();
?>
	<section id="blog">
		<a href="<?php echo get_site_url(); ?>/blog" class="cover">
			<div class="filter">
				<div class="container valign-wrapper">
					<h1><Strong>BLOG</Strong></h1>
				</div>
			</div>
		</a>
		<div id="categories" class="red lighten-1">
	        <div class="all">
	        	<?php
	        		$categories = get_categories(array(
					    'orderby' => 'name',
					    'parent'  => 0
					));

					foreach ($categories as $category) {
					    printf('<a href="%1$s" class="category">%2$s</a>',
					        esc_url(get_category_link($category->term_id)),
					        esc_html($category->name)
					    );
					}
	        	?>
	        </div>
	    </div>
	    <!-- postagem -->
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="container">
			<article class="postagem clearfix">
				<h2 class="title"><strong><?php the_title(); ?></strong></h2>
				<div class="autor clearfix">
					<div class="col m6">
	                    <div class="left">
	                        <?php echo get_avatar(get_the_author_meta('ID'), 42); ?>
	                    </div>
	                    <div class="left">
	                        <p>Escrito por
	                        	<span>
	                        		<?php
	                        			$first = get_the_author_meta('first_name');
	                        			$last = get_the_author_meta('last_name');
		                        		if($first == '' && $last == '') {
		                        			the_author();
		                        		} else {
		                        			echo $first;
		                        			echo ' '.$last;
		                        		}
	                        		?>
	                        	</span>
	                        </p>
	                        <p class="time"><?php the_time('d M, Y') ?> | <?php the_time('G:i'); ?></p>
	                    </div>
	                </div>
	                <div class="col m6 share">
	                	<div class="right">
					        <a href="javascript:fbShare('<?php echo get_permalink(); ?>, 560, 650')" class="share-fb" title="Compartilhe com seus amigos no Facebook!"></a>
					        <a href="https://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo get_permalink(); ?>" class="share-tw" title="Compartilhe com seus amigos no Twitter!" target="_blank"></a>
					        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php the_excerpt();?>&source=" class="share-li" title="Compartilhe com seus amigos no Linkedin" target="_blank"></a>
	                	</div>
	                </div>
                </div>
                <div class="texto">
                	<?php the_content(); ?>
                </div>
			    <div class="share-side">
			        <a href="javascript:fbShare('<?php echo get_site_url(); ?>, 560, 650')" class="share-fb" title="Compartilhe com seus amigos no Facebook!"></a>
			        <a href="https://twitter.com/share?text=<?php the_title(); ?>&url=<?php echo get_permalink(); ?>" class="share-tw" title="Compartilhe com seus amigos no Twitter!" target="_blank"></a>
			        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php the_excerpt();?>&source=" class="share-li" title="Compartilhe com seus amigos no Linkedin" target="_blank"></a>
			    </div>
                <div id="disqus_thread"></div>
                <script>
                    /**
                    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                    /*
                    var disqus_config = function () {
                    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };
                    */

                    var disqus_config = function(){ this.language = 'pt'; }
                    var disqus_shortname = "aggrandize";
                    var disqus_title = "<?php the_title(); ?>";
                    var disqus_url = "<?php the_permalink(); ?>";
                    console.log(disqus_url);
                    var disqus_identifier = "aggrandize-<?php echo $post->ID; ?>";

                    (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = '//aggrandize.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
			</article>
		</div>
		<?php endwhile; ?>
		<?php else : ?>
		<?php endif; ?>
		<!-- fim postagem -->
		<section id="solucao-contato">
			<div class="container">
				<h2>Quer saber mais?</h2>
				<a href="<?php echo get_site_url(); ?>/contato" class="btn waves-effect waves-red btn-large red lighten-1">ENTRE EM CONTATO</a>
			</div>
		</section>
	</section>
	<script>
	    function fbShare(url, winWidth, winHeight) {
	        var winTop = (screen.height / 2) - (winHeight / 2);
	        var winLeft = (screen.width / 2) - (winWidth / 2);
	        window.open('http://www.facebook.com/sharer.php?s=100&u='+url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
	    }
	</script>
<?php get_footer(); ?>