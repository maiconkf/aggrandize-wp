		<footer id="footer" class="page-footer red lighten-1 clearfix">
			<div class="container">
				<div class="col l4 m6 s12">
					<img src="<?php echo get_template_directory_uri() ?>/dist/img/Aggrandize-Logo.png" class="responsive-img" width="300" height="137">
				</div>
				<div class="col l7 m6 offset-l1 s12 links">
					<h5 class="white-text"><strong>Soluções 100% Oracle</strong></h5>
					<ul class="col l4 s5">
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/middleware"><span>&rsaquo;</span> Middleware</a></li>
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/banco-de-dados"><span>&rsaquo;</span> Banco de Dados</a></li>
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/healthcheck"><span>&rsaquo;</span> HealthCheck</a></li>
					</ul>
					<ul class="col l4 s4">
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/monitoracao"><span>&rsaquo;</span> Monitoração</a></li>
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/integracao"><span>&rsaquo;</span> Integração</a></li>
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/flexdeploy"><span>&rsaquo;</span> FlexDeploy</a></li>
					</ul>
					<ul class="col l4 s3">
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/ebs"><span>&rsaquo;</span> EBS</a></li>
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/siebel"><span>&rsaquo;</span> Siebel</a></li>
						<li><a class="grey-text text-lighten-3" href="<?php echo get_site_url(); ?>/solucoes/devops"><span>&rsaquo;</span> DevOps</a></li>
					</ul>
				</div>
			</div>
			<div class="footer-copyright clearfix">
				<div class="container center">
				<span class="center">© Aggrandize 2017 – Todos os direitos reservados</span>
				<a class="grey-text text-lighten-4 right" href="http://maiconfurtado.com.br" target="_blank"><strong>mf</strong></a>
				</div>
			</div>
		</footer>
  		<script src="<?php echo get_template_directory_uri() ?>/dist/js/scripts.min.js"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-103150961-1', 'auto');
			ga('send', 'pageview');
		</script>
  	</body>
</html>