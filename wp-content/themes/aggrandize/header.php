<?php
	$class = '';
	if(is_page()) {
		global $post;
		$parents = get_post_ancestors($post->ID);
		$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
	    $parent = get_post($id);
		$class = $parent->post_name;
	}
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title('|', true,'right'); ?></title>
		<?php if (have_posts() && is_single()) :?>
			<?php
	            if (!has_excerpt()) {
	                $excerpt = wp_trim_words( $post->post_content, 40, '...' );
	            } else {
	                $excerpt = excerpt(40);
	            }

	            if (has_post_thumbnail()) {
					$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "large" )[0];
				} else {
					$img = get_template_directory_uri().'/dist/img/bg-sem-foto.png';
				}
			?>
            <meta name="description" content="<?= $excerpt; ?>">
			<meta property="og:image" content="<?= $img ?>" />
            <meta property="og:description" content="<?= $excerpt; ?>">
            <meta property="og:url" content="<?php echo the_permalink(); ?>"/>
            <meta property="og:title" content="<?php echo the_title(); ?>"/>
		<?php else : ?>
			<meta name="description" content="<?php bloginfo('description'); ?>" />
		<?php endif; ?>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/dist/css/styles.min.css">
		<?php if($class == 'solucoes') { echo '<link rel="stylesheet" href="'.plugins_url().'/testimonial-rotator/testimonial-rotator-style.css">'; } ?>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
		<link href="https://fonts.googleapis.com/css?family=Material+Icons|Open+Sans:400,700|Titillium+Web:200" rel="stylesheet">
		<!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
	</head>
	<body>
		<header id="header" class="navbar-fixed">
			<nav class="red lighten-1">
				<div class="nav-wrapper container">
					<a href="<?php echo get_site_url(); ?>" class="brand-logo"><img src="<?php echo get_template_directory_uri() ?>/dist/img/Aggrandize.png" width="107" height="42"></a>
					<a href="javascript:;" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<ul id="nav-mobile" class="right hide-on-med-and-down">
						<li><a href="<?php echo get_site_url(); ?>">Início</a></li>
						<li><a href="<?php echo get_site_url(); ?>/quem-somos">Quem Somos</a></li>
						<li><a href="<?php echo get_site_url(); ?>/solucoes" class="dropdown-menu" data-type="solucoes">Soluções</a></li>
						<li><a href="<?php echo get_site_url(); ?>/blog">Blog</a></li>
						<li><a href="<?php echo get_site_url(); ?>/contato">Contato</a></li>
					</ul>
					<ul class="side-nav" id="mobile-demo">
						<li><a href="<?php echo get_site_url(); ?>">Início</a></li>
						<li><a href="j<?php echo get_site_url(); ?>/quem-somos">Quem Somos</a></li>
						<li><a href="<?php echo get_site_url(); ?>/solucoes">Soluções</a></li>
						<li><a href="<?php echo get_site_url(); ?>/blog">Blog</a></li>
						<li><a href="<?php echo get_site_url(); ?>/contato">Contato</a></li>
					</ul>
					<div class="sub-nav solucoes red lighten-1">
						<h3><strong>SOLUÇÕES 100% ORACLE</strong></h3>
						<div class="col m4">
							<ul>
								<li><a href="<?php echo get_site_url(); ?>/solucoes/middleware" class="grey-text text-lighten-3"><span>&rsaquo;</span> Middleware</a></li>
								<li><a href="<?php echo get_site_url(); ?>/solucoes/devops" class="grey-text text-lighten-3"><span>&rsaquo;</span> DevOps</a></li>
								<li><a href="<?php echo get_site_url(); ?>/solucoes/siebel" class="grey-text text-lighten-3"><span>&rsaquo;</span> Siebel</a></li>
							</ul>
						</div>
						<div class="col m4 w126 second">
							<li><a href="<?php echo get_site_url(); ?>/solucoes/healthcheck" class="grey-text text-lighten-3"><span>&rsaquo;</span> HealthCheck</a></li>
							<li><a href="<?php echo get_site_url(); ?>/solucoes/flexdeploy" class="grey-text text-lighten-3"><span>&rsaquo;</span> FlexDeploy</a></li>
							<li><a href="<?php echo get_site_url(); ?>/solucoes/ebs" class="grey-text text-lighten-3"><span>&rsaquo;</span> EBS</a></li>
						</div>
						<div class="col m4 w126 right">
							<ul>
								<li><a href="<?php echo get_site_url(); ?>/solucoes/banco-de-dados" class="grey-text text-lighten-3"><span>&rsaquo;</span> Banco de Dados</a></li>
								<li><a href="<?php echo get_site_url(); ?>/solucoes/monitoracao" class="grey-text text-lighten-3"><span>&rsaquo;</span> Monitoração</a></li>
								<li><a href="<?php echo get_site_url(); ?>/solucoes/integracao" class="grey-text text-lighten-3"><span>&rsaquo;</span> Integração</a></li>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		</header>