<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php wp_title('|', true,'right'); ?></title>
		<meta name="description" content="<?php bloginfo('description'); ?>" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/dist/css/styles.min.css">
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
		<link href="https://fonts.googleapis.com/css?family=Material+Icons|Open+Sans:400,700|Titillium+Web:200" rel="stylesheet">
		<!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
	</head>
	<body id="page404">
		<div class="page404">
			<div class="container">
				<div class="col-12 center-block clearfix box">
					<div class="col-12 center-block center">
						<hgroup>
							<h1>OOPS!</h1>
							<h2>404 - Página não encontrada!</h2>
						</hgroup>
						<div class="col-9 center-block">
							<p>Parece que essa página realmente não existe. Mas não se preocupe! Só clicar no botão abaixo que você irá pra nossa página inicial:</p>
						</div>
						<a href="<?php echo get_site_url(); ?>" class="btn waves-effect waves-red btn-large red lighten-1">PÁGINA INICIAL</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>