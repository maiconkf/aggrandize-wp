<?php
	/* Template name: Quem Somos */
	get_header();
?>
	<div id="quem-somos">
		<div class="cover">
			<div class="filter">
				<div class="container valign-wrapper">
					<h1><Strong>TEMOS O JEITO PARA LIDAR COM INFRAESTRUTURA ORACLE</Strong></h1>
				</div>
			</div>
		</div>
	  	<section id="content" class="clearfix">
			<div class="container">
				<div class="col m11 center-block">
			  		<hgroup class="center">
			  			<h2 class="center title">VOCAÇÃO</h2>
			  			<h3 class="subtitle">Somos pessoas, com capacidade técnica, motivadas e comprometidas com o ecossistema em que estamos inseridos.</h3>
			  		</hgroup>
					<div class="separator"></div>
			  		<hgroup class="center">
			  			<h2 class="center title">HISTÓRICO</h2>
			  			<h3 class="subtitle">A Aggrandize foi fundada em fevereiro de 2015 por sócios que acreditam que uma empresa de NICHO agrega MAIS valor aos clientes. É por isso que nossa visão é: <em>“Sermos a melhor empresa de serviços em infraestrutura Oracle do Brasil”</em>.</h3>
			  		</hgroup>
					<div class="separator"></div>
					<div class="socios clearfix">
				  		<hgroup class="center">
				  			<h2 class="center title">SÓCIOS</h2>
				  		</hgroup>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/alvaro-morais-jr-8a32b926" target="_blank" title="Clique e veja o Linkedin do Alvaro">
				  				<p class="nome"><strong>Alvaro Morais Jr</strong></p>
								<p class="funcao"><em>Consultor Siebel</em></p>
							</a>
				  		</div>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/augustomauricio" target="_blank" title="Clique e veja o Linkedin do Augusto">
					  			<p class="nome"><strong>Augusto Mauricio</strong></p>
								<p class="funcao"><em>Comercial</em></p>
							</a>
				  		</div>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/diego-siegert-de-oliveira-19103931" target="_blank" title="Clique e veja o Linkedin do Diego">
					  			<p class="nome"><strong>Diego Siegert</strong></p>
								<p class="funcao"><em>Consultor de Integração</em></p>
							</a>
				  		</div>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/diogodeandrade" target="_blank" title="Clique e veja o Linkedin do Diogo">
					  			<p class="nome"><strong>Diogo de Andrade</strong></p>
								<p class="funcao"><em>Consultor de Monitoração</em></p>
							</a>
				  		</div>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/eislervoigt" target="_blank" title="Clique e veja o Linkedin do Eisler">
					  			<p class="nome"><strong>Eisler Voigt</strong></p>
								<p class="funcao"><em>Comercial</em></p>
							</a>
				  		</div>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/francisco-cavalin" target="_blank" title="Clique e veja o Linkedin do Francisco">
					  			<p class="nome"><strong>Francisco Cavalin</strong></p>
								<p class="funcao"><em>Consultor de Banco de Dados</em></p>
							</a>
				  		</div>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/mariojmendoza" target="_blank" title="Clique e veja o Linkedin do Mario">
					  			<p class="nome"><strong>Mario Mendoza</strong></p>
								<p class="funcao"><em>Consultor Middleware</em></p>
							</a>
				  		</div>
				  		<div class="col m3 s6 center">
				  			<a href="https://www.linkedin.com/in/rodrigo-kickhofel" target="_blank" title="Clique e veja o Linkedin do Rodrigo">
					  			<p class="nome"><strong>Rodrigo Kickhöfel</strong></p>
								<p class="funcao"><em>Consultor DevOps</em></p>
							</a>
				  		</div>
				  	</div>
					<div class="separator"></div>
			  		<hgroup class="center">
			  			<h2 class="center title">CLIENTES</h2>
			  			<h3 class="subtitle">Temos em nosso portifólio clientes que querem mais do que um serviço de infraestrutura Oracle. Dentre esses, trabalhamos com players no mercado de TELECOM, VAREJO e LOGÍSTICA, além de outros segmentos.<span></h3>
			  		</hgroup>
			  	</div>
		  	</div>
	  	</section>
		<section id="solucao-contato">
			<div class="container">
				<div class="col m12 l10 center-block">
					<h2>Saiba o motivo por qual nossos clientes nos indicam</h2>
				    <a href="<?php echo get_site_url(); ?>/contato" class="btn waves-effect waves-red btn-large red lighten-1">ENTRAR EM CONTATO</a>
				</div>
			</div>
		</section>
  	</div>
<?php get_footer(); ?>