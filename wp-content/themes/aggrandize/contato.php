<?php
	/* Template name: Contato */
	get_header();
?>
	<section id="contato">
		<div class="cover">
			<div class="filter">
				<div class="container valign-wrapper">
					<h1><Strong>Queremos sanar todas as suas dúvidas</Strong></h1>
				</div>
			</div>
		</div>
		<?php
			if (isset($_POST['send'])) {
				//Variaveis de POST, Alterar somente se necessário
				//====================================================
				$nome = $_POST['your_name'];
				$email = $_POST['your_email'];
				$tel = $_POST['your_telefone'];
				$mensagem = $_POST['your_message'];
				//====================================================

				//REMETENTE --> ESTE EMAIL TEM QUE SER VALIDO DO DOMINIO
				//====================================================
				$email_remetente = "comercial@aggrandize.com.br"; // deve ser uma conta de email do seu dominio
				//====================================================

				//Configurações do email, ajustar conforme necessidade
				//====================================================
				$email_destinatario = "comercial@aggrandize.com.br"; // pode ser qualquer email que receberá as mensagens
				$email_reply = "$email";
				$email_assunto = "Contato via site"; // Este será o assunto da mensagem
				//====================================================

				//Monta o Corpo da Mensagem
				//====================================================
				$email_conteudo = "Nome = $nome \n";
				$email_conteudo .= "Email = $email \n";
				$email_conteudo .= "Telefone = $tel \n";
				$email_conteudo .= "Mensagem = $mensagem \n";
				//====================================================

				//Seta os Headers (Alterar somente caso necessario)
				//====================================================
				$email_headers = implode ( "\n",array ( "From: Aggrandize <$email_remetente>", "Reply-To: $email_reply", "Return-Path: $email_remetente","MIME-Version: 1.0","X-Priority: 3","Content-Type: text/html; charset=UTF-8" ) );
				//====================================================

				//Enviando o email
				//====================================================
				if (wp_mail($email_destinatario, $email_assunto, nl2br($email_conteudo), $email_headers)){
					echo "<script>alert('Email enviado com sucesso!');</script>";
				} else{
					echo "<script>alert('Erro ao enviar email.')</script>";
				}
				//====================================================
			}
		?>
		<div class="full clearfix">
			<div class="col m6 left-block">
				<div class="container right">
					<hgroup>
						<h2 class="title-section">FALE CONOSCO</h2>
						<h3 class="title-bigger">Contate-nos</h3>
					</hgroup>
					<p class="desc-section">Nós adoraríamos ler sua mensagem. Se você tem alguma dúvida, comentário ou sugestão, sinta-se a vontade de contatar-nos. Nós responderemos o seu email o mais rápido possível.</p>
					<div class="sub-content">
						<!--
            <h3><i class="material-icons left">place</i> <span>Endereço</span></h3>
						<p>Rua 7 de Setembro, 60 - Pelotas-RS, 96015-300</p>
						<h3 class="outro"><i class="material-icons left">call</i> <span>Telefone</span></h3>
						<p>(53) 3333-3333</p>
            -->
						<h3 class="outro"><i class="material-icons left">email</i> <span class="email">Email</span></h3>
						<p>comercial@aggrandize.com.br</p>
					</div>
				</div>
			</div>
			<div class="col m6 bg right">
				<div class="box">
					<p class="desc-section">Entre em contato usando o formulário abaixo. Nossa equipe responderá você dentro de algumas horas.</p>
					<p class="error-alert contact center"></p>
					<form method="post">
						<div class="input-field">
							<input name="your_name" id="nome" type="text" class="validate" required>
							<label for="nome">Nome</label>
						</div>
						<div class="input-field">
							<input name="your_email" id="email" type="email" class="validate" required>
							<label for="email">Email</label>
						</div>
						<div class="input-field">
							<input name="your_telefone" id="tel" type="text" class="validate" required>
							<label for="tel">Telefone</label>
						</div>
						<div class="input-field">
							<textarea name="your_message" id="message" class="materialize-textarea validate" required></textarea>
							<label for="message">Mensagem</label>
						</div>
						 <button class="btn waves-effect waves-red btn-large red lighten-1" type="submit" name="send">Enviar</button>
					</form>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>