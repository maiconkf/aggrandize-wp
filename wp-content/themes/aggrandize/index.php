<?php if (have_posts()) :?>
<?php
	/* Template name: Blog */
	get_header();
?>
	<section id="blog">
		<div class="cover">
			<div class="filter">
				<div class="container valign-wrapper">
					<h1><Strong>BLOG</Strong></h1>
				</div>
			</div>
		</div>
		<div id="categories" class="red lighten-1">
	        <div class="all">
	        	<?php
	        		$categories = get_categories(array(
					    'orderby' => 'name',
					    'parent'  => 0
					));

					foreach ($categories as $category) {
					    printf('<a href="%1$s" class="category">%2$s</a>',
					        esc_url(get_category_link($category->term_id)),
					        esc_html($category->name)
					    );
					}
	        	?>
	        </div>
	    </div>
	    <div id="posts" class="container">
	    	<?php while (have_posts()) : the_post(); ?>
				<article class="col m12 l9 center-block">
					<a href="<?php the_permalink(); ?>" class="box clearfix">
						<div class="col m5 pic">
							<?php
								if (has_post_thumbnail()) {
									the_post_thumbnail('thumbnail');
								} else {
									echo '<img src="'.get_template_directory_uri().'/dist/img/bg-sem-foto.png" class="responsive-img">';
								}
							?>
						</div>
						<div class="col m7 content">
							<h1><strong><?php the_title(); ?></strong></h1>
							<p>
								<?php
									if(wp_is_mobile()) {
										echo excerpt(40);
									} else {
										echo excerpt(60);
									}
								?></p>
							<span>LEIA MAIS</span>
						</div>
					</a>
				</article>
			<?php endwhile; ?>
	    </div>
	    <div class="container">
			<?php afc_paginacao(); ?>
		</div>
		<section id="solucao-contato">
			<div class="container">
				<h2>Quer saber mais?</h2>
				<a href="<?php echo get_site_url(); ?>/contato" class="btn waves-effect waves-red btn-large red lighten-1">ENTRE EM CONTATO</a>
			</div>
		</section>
	</section>
<?php get_footer(); ?>
<?php else : ?>
	<?php require_once('404.php'); ?>
<?php endif; ?>