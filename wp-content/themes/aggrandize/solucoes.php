<?php
	/* Template name: Soluções */
	get_header();
?>
	<div id="solucoes">
		<div class="cover">
			<div class="filter">
				<div class="container valign-wrapper">
					<h1><Strong>SOLUÇÕES 100% ORACLE</Strong></h1>
				</div>
			</div>
		</div>
	  	<section id="content" class="clearfix">
		  	<div class="container">
				<div class="col s12 m6 l4 solucao solucao-1">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-middleware.png" class="responsive-img">
							<span class="card-title">Middleware</span>
						</div>
						<div class="card-content">
							<p>&#9679; Resiliência em Oracle Fusion Middleware</p><p>&#9679; Experiência em Ambientes de Grande Porte</p><p>&#9679; Equipe Certificada</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/middleware">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-2">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-banco-de-dados.png" class="responsive-img">
							<span class="card-title">Banco de Dados</span>
						</div>
						<div class="card-content">
							<p>&#9679; Conhecimento em todas as versões de banco de dados</p><p>&#9679; Projetos de todos os portes em diversos segmentos</p><p>&#9679; Equipe Certificada</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/banco-de-dados">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-3">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-siebel.png" class="responsive-img">
							<span class="card-title">Siebel</span>
						</div>
						<div class="card-content">
							<p>&#9679; Migrações de versão e apoio para equipe projetos</p><p>&#9679; Projetos diferenciados com Oracle Coherence</p><p>&#9679; Equipe com amplo conhecimento em infraestrutura e no produto</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/siebel">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-4">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-monitoracao.png" class="responsive-img">
							<span class="card-title">Monitoração</span>
						</div>
						<div class="card-content">
							<p>&#9679; Implantação de OEM para todos os produtos de Infraestrutura</p><p>&#9679; Produto próprio para visão unificada do ambiente de Middleware e Banco de Dados</p><p>&#9679; Equipe certificada em OEM e nos produtos alvo</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/monitoracao">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-5">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-integracao.png" class="responsive-img">
							<span class="card-title">Integração</span>
						</div>
						<div class="card-content">
							<p>&#9679; Diversos produtos e formas para integração de dados e aplicações</p><p>&#9679; Equipe com participação em cases Oracle</p><p>&#9679; Equipe certificada em ODI, SOA e BPM</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/integracao">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-6">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-ebs.png" class="responsive-img">
							<span class="card-title">EBS</span>
						</div>
						<div class="card-content">
							<p>&#9679; Mais de 10 anos de experiência na sustentação de Oracle EBS</p><p>&#9679; Produto para DevOps de Oracle EBS</p><p>&#9679; Equipe certificada</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/ebs">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-7">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-healthcheck.png" class="responsive-img">
							<span class="card-title">HealthCheck</span>
						</div>
						<div class="card-content">
							<p>&#9679; As principais variáveis de controle de seu ambiente de middleware Oracle em uma única visão</p><p>&#9679; Simples, sem agenda no servidor alvo e não intrusivo</p><p>&#9679; Utilize por 2 meses sem custo</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/healthcheck">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-8">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-devops.png" class="responsive-img">
							<span class="card-title">DevOps</span>
						</div>
						<div class="card-content">
							<p>&#9679; Solução completa de DevOps, Continuous Delivery e Release Automation para Oracle</p><p>&#9679; Experiência em cases de grande porte</p><p>&#9679; Equipe com skiils de infraestrutura, testes e desenvolvimento de software</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/devops">Saiba mais</a>
						</div>
					</div>
	        	</div>
				<div class="col s12 m6 l4 solucao solucao-9">
					<div class="card">
						<div class="card-image">
							<img src="<?php echo get_template_directory_uri() ?>/dist/img/solucao-flexdeploy.png" class="responsive-img">
							<span class="card-title">FlexDeploy</span>
						</div>
						<div class="card-content">
							<p>&#9679; Plugins para Oracle Fusion Middleware, Banco de dados e aplicações Oracle</p><p>&#9679; DevOps para EBS</p><p>&#9679; Maior ROI para DevOps em ambiente Oracle</p>
						</div>
						<div class="card-action">
							<a href="<?php echo get_site_url(); ?>/solucoes/flexdeploy">Saiba mais</a>
						</div>
					</div>
	        	</div>
	        </div>
	  	</section>
  	</div>
<?php get_footer(); ?>