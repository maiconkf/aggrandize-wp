<?php if (have_posts()) :?>
<?php
	/* Template name: Solução */
	get_header();
?>
	<?php while (have_posts()) : the_post(); ?>
	<div id="solucao">
		<div class="cover" style="background-image: url(<?php the_post_thumbnail_url() ?>);">
			<div class="filter">
				<div class="container valign-wrapper">
					<h1><Strong><?php the_title() ?></Strong></h1>
				</div>
			</div>
		</div>
	  	<section id="content" class="clearfix">
	  		<div class="container">
				<div class="col m10 l9 center-block block-content">
		  			<?php the_content() ?>
					<?php endwhile; ?>
			  	</div>
			</div>
			<section id="solucao-contato">
				<div class="container">
					<h2>Quer saber mais?<br>Entre em contato conosco!</h2>
					<button type="button" class="btn waves-effect waves-red btn-large red lighten-1">ENTRAR EM CONTATO</button>
				</div>
			</section>
	  	</section>
	</div>
	<div id="modal">
		<div class="fade close"></div>
		<div class="box">
			<a href="javascript:;" class="close pull-right">
				<svg fill="#898989" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
				    <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
				    <path d="M0 0h24v24H0z" fill="none"/>
				</svg>
			</a>
			<div class="content">
				<form method="post" class="col m12 s12 center-block">
					<div class="input-field">
						<input name="your_name" id="nome" type="text" class="validate" required>
						<label for="nome">Nome</label>
					</div>
					<div class="input-field">
						<input name="your_email" id="email" type="email" class="validate" required>
						<label for="email">Email</label>
					</div>
					<div class="input-field">
						<input name="your_telefone" id="tel" type="text" class="validate" required>
						<label for="tel">Telefone</label>
					</div>
					<div class="input-field">
						<input name="your_subject" id="sub" type="text" class="validate" value="Solução - <?php the_title(); ?>" required>
						<label for="sub">Assunto</label>
					</div>
					<div class="input-field">
						<textarea name="your_message" id="message" class="materialize-textarea validate" required></textarea>
						<label for="message">Mensagem</label>
					</div>
					 <button class="btn waves-effect waves-red btn-large red lighten-1" type="submit" name="send">Enviar</button>
				</form>
			</div>
		</div>
	</div>
	<?php
		if (isset($_POST['send'])) {
			//Variaveis de POST, Alterar somente se necessário
			//====================================================
			$nome = $_POST['your_name'];
			$email = $_POST['your_email'];
			$tel = $_POST['your_telefone'];
			$assunto = $_POST['your_subject'];
			$mensagem = $_POST['your_message'];
			//====================================================

			//REMETENTE --> ESTE EMAIL TEM QUE SER VALIDO DO DOMINIO
			//====================================================
			$email_remetente = "comercial@aggrandize.com.br"; // deve ser uma conta de email do seu dominio
			//====================================================

			//Configurações do email, ajustar conforme necessidade
			//====================================================
			$email_destinatario = "comercial@aggrandize.com.br"; // pode ser qualquer email que receberá as mensagens
			$email_reply = "$email";
			$email_assunto = "$assunto"; // Este será o assunto da mensagem
			//====================================================

			//Monta o Corpo da Mensagem
			//====================================================
			$email_conteudo = "Nome = $nome \n";
			$email_conteudo .= "Email = $email \n";
			$email_conteudo .= "Telefone = $tel \n";
			$email_conteudo .= "Assunto = $assunto \n";
			$email_conteudo .= "Mensagem = $mensagem \n";
			//====================================================

			//Seta os Headers (Alterar somente caso necessario)
			//====================================================
			$email_headers = implode ( "\n",array ( "From: Aggrandize <$email_remetente>", "Reply-To: $email_reply", "Return-Path: $email_remetente","MIME-Version: 1.0","X-Priority: 3","Content-Type: text/html; charset=UTF-8" ) );
			//====================================================

			//Enviando o email
			//====================================================
			if (wp_mail($email_destinatario, $email_assunto, nl2br($email_conteudo), $email_headers)){
				echo "<script>alert('Email enviado com sucesso!');</script>";
			} else{
				echo "<script>alert('Erro ao enviar email.')</script>";
			}
			//====================================================
		}
	?>
<?php get_footer(); ?>
<?php else : ?>
	<?php require_once('404.php'); ?>
<?php endif; ?>