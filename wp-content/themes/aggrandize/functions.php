<?php
	add_theme_support('post-thumbnails');

	function your_excerpt_length( $length ) {
		return 200;
	}
	add_filter( 'excerpt_length', 'your_excerpt_length' );

	function excerpt($limit) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt)>=$limit) {
			array_pop($excerpt);
			$excerpt = implode(" ",$excerpt).'...';
		} else {
			$excerpt = implode(" ",$excerpt);
		}
		$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
		return $excerpt;
	}

	function filter_ptags_on_images($content){
	    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	}
	add_filter('the_content', 'filter_ptags_on_images');

	function wp_remove_h1_from_editor( $init ) {
	    $init['block_formats'] = 'Paragrafo=p;Cabeçalho 2=h2;Cabeçalho 3=h3;Cabeçalho 4=h4;Cabeçalho 5=h5;Cabeçalho 6=h6;Pré-formatado=pre;';
	    return $init;
	}
	add_filter( 'tiny_mce_before_init', 'wp_remove_h1_from_editor' );

	function afc_paginacao($pages = '', $range = 3) {
		$showitems = ($range * 2)+1;
		global $paged;
		if(empty($paged)) $paged = 1;

		if($pages == '') {
			global $wp_query;
			$pages = $wp_query->max_num_pages;

			if(!$pages) {
				$pages = 1;
			}
		}

		if (1 != $pages) {
			echo "<div class='paginacao'>";
			if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "";
			if($paged > 3 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'><strong>1</strong></a> <span class='dots'><strong>...</strong></span>";

			for ($i=1; $i <= $pages; $i++) {
				if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
			    	echo ($paged == $i)? "<span class='current'><strong>".$i."</strong></span>":"<a href='".get_pagenum_link($i)."' class='inactive'><strong>".$i."</strong></a>";
			 	}
			}

			if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<span class='dots'><strong>...</strong></span> <a href='".get_pagenum_link($pages)."'><strong>$pages</strong></a>";
			if ($paged < $pages && $showitems < $pages) echo "";
			echo "</div>";
		}
	}

	function wpdocs_theme_name_wp_title( $title, $sep ) {
	    if (is_feed()) {
	        return $title;
	    }

	    global $page, $paged;

	    // Add the blog name
	    $title .= get_bloginfo('name', 'display');
	    // Add a page number if necessary:
	    if (($paged >= 2 || $page >= 2) && ! is_404()) {
	        $title .= " $sep " . sprintf( __('Página %s', '_s'), max($paged, $page));
	    }
	    return $title;
	}
	add_filter( 'wp_title', 'wpdocs_theme_name_wp_title', 10, 2 );
?>